#include <bits/stdc++.h>
using namespace std;

bool bad(int a)
{
	return (a + rand()) % 2;
}

int main()
{
	srand(23);

	typedef list<int> list_t;
	list_t l = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

// Задание 3
// Если предикат bad вернёт true, то из списка удалится элемент по итератору, и итератор инвалидируется.
// Потом выполнится инкремент на инвалидированном итераторе, но так делать нельзя
// Цикл можно переписать так:

for (list_t::iterator it = l.begin(); it != l.end(); ) {
	if (bad(*it)) {
		it = l.erase(it);
	} else {
		++it;
	}
}


// ---------

	for (int x : l)
		cout << x << " ";
	
	return 0;
}
