#include <bits/stdc++.h>

// Задание 11

class TInputSortedStringStream {
public:
	virtual ~TInputSortedStringStream() = default;
	virtual bool ReadLine(std::string &out) = 0;
};

class TInputSortedStringStreamSet : public TInputSortedStringStream {
	using pair_t = std::pair<std::string, TInputSortedStringStream*>;
	// Очередь с приоритетом обеспечивает вставку и удаление наибольшего элемента за O(log(n)).
	// В ней хранятся пары (следующая строка в потоке, указатель на поток).
	std::priority_queue<pair_t, std::vector<pair_t>, std::greater<pair_t>> queue;

public:
	virtual bool ReadLine(std::string &out) override {
		if (queue.empty())
			return false;

		pair_t top = queue.top();
		queue.pop();
		// swap не вызовет копирование больших строк.
		out.swap(top.first);
		if (top.second->ReadLine(top.first)) {
			// Явно избегаем копирования пары.
			queue.push(std::move(top));
		}
		return true;
	}

	// Переданный аргумент должен оставаться живым до конца жизни текущего TInputSortedStringStreamSet.
	// Можно удобно собирать вызовы AddStream в цепочки.
	virtual TInputSortedStringStreamSet &AddInput(TInputSortedStringStream *stream) {
		std::string str;
		if (stream->ReadLine(str)) {
			// Явно избегаем копирования строки.
			queue.push(make_pair(std::move(str), stream));
		}
		return *this;
	}
};


// ----------

class FileInputStringStream : public TInputSortedStringStream {
	std::ifstream in;

public:
	// Строки в файле должны быть отсортированы.
	FileInputStringStream(std::string file) : in(file) {
	}

	virtual bool ReadLine(std::string &out) override {
		std::getline(in, out);
		return (bool) in;
	}
};

int main()
{
	FileInputStringStream i1("s1");
	FileInputStringStream i2("s2");
	FileInputStringStream i3("s3");

	TInputSortedStringStreamSet input;
	input
			.AddInput(&i1)
			.AddInput(&i2)
			.AddInput(&i3);

	std::string str;
	while (input.ReadLine(str)) {
		std::cout << str << "\n";
	}
	
	return 0;
}
