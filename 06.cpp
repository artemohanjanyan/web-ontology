// Задание 6
// Если требуется создавать объекты на куче, то, вероятнее всего, имеет смысл воспользоваться умными указателями для обращения с ними.
// Код, использующий обычные указатели для управления объектами на куче, подвержен ошибкам.
// Например, можно забыть удалить его, особенно если есть несколько точек выхода из области владения указателем (return/break, исключения).
//
// Для простых случаев, когда доступ к ресурсу происходит одновременно только внутри одного блока кода или объекта,
// можно использовать std::unique_ptr. Жизненный цикл объекта будет отслеживаться автоматически.
// Если же доступ к объекту требуется из разных частей программы (или нескольких других объектов) одновременно,
// и его жизненный цикл более сложный, то есть смысл использовать shared_ptr.
//
// Например, shared_ptr можно использовать в реализации оптимизации copy-on-write.
// Можно хранить указатель на тяжёлую часть объекта в shared_ptr, и при его модификации копировать его,
// если shared_ptr::use_count() больше единицы. Пример реализации арифметики длинных чисел с данной оптимизацией:
// https://github.com/artemohanjanyan/university/blob/master/2_term/languages/bigint/digit_array.h#L102-L106


// ---------
