#include <bits/stdc++.h>

template <class TObject>
class TCopyContainer {
	std::forward_list<std::unique_ptr<TObject>> storage;

public:
	TObject *AddItem(const TObject &obj) {
		storage.push_front(std::make_unique<TObject>(obj));
		return storage.begin()->get();
	}
};

// Пример реализации
template <class T>
class TSortedReader {
	std::string str;
	std::ifstream in;
	TCopyContainer<T> storage;

public:
	// Данные в файле должны быть отсортированы.
	TSortedReader(const std::string &file)
			: str(file), in(file) {
	}

	void Reset() {
		in.close();
		in.clear();
		in.open(str);
	}

	const T *Next() {
		T t;
		return (in >> t) ? storage.AddItem(t) : nullptr;
	}
};

// Задание 14

// Используется алгоритм, аналогичный алгоритму merge из merge sort.
template <class T>
size_t CountCommon(TSortedReader<T> &a, TSortedReader<T> &b) {
	const T *aIt = a.Next(), *bIt = b.Next();
	size_t count = 0;

	while (aIt != nullptr && bIt != nullptr) {
		if (*aIt < *bIt) {
			aIt = a.Next();
		} else if (*bIt < *aIt) {
			bIt = b.Next();
		} else {
			// Если !(*aIt < *bIt) && !(*bIt < *aIt), то *aIt == *bIt.
			++count;
			aIt = a.Next();
			bIt = b.Next();
		}
	}

	return count;
}


// ----------


int main()
{
	TSortedReader<std::string> a("s1"), b("s2");

	std::cout << CountCommon(a, b);

	return 0;
}
