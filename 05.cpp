#include <bits/stdc++.h>

class StreamExhaustedException : public std::exception {
public:
	virtual const char* what() const noexcept override {
		return "stream chain is exhausted";
	}
};

//if (streamQueue.empty()) {
//	throw StreamExhaustedException{};
//}

// Задание 5

class TInputStringStream {
public:
	virtual ~TInputStringStream() = default;
	virtual bool ReadLine(std::string &out) = 0;
};

class TInputStringStreamChain : public TInputStringStream {
	// Для последовательного считывания используется очередь.
	std::queue<TInputStringStream*> streamQueue;

public:
	virtual bool ReadLine(std::string &out) override {
		// Пропустить все пустые TInputStringStream.
		while (!streamQueue.empty() && !streamQueue.front()->ReadLine(out)) {
			streamQueue.pop();
		}
		// Если нашёлся хотя бы один не пустой, то к этой строчке в out уже записалось считанное значение.
		if (streamQueue.empty())
			return false;
		return true;
	}

	// Возвращается ссылка на текущий TInputStringStreamChain, так можно удобно собирать вызовы AddStream в цепочки.
	virtual TInputStringStreamChain& AddStream(TInputStringStream *stream) {
		streamQueue.push(stream);
		return *this;
	}
};


// ---------

class StdInInputStringStream : public TInputStringStream {
public:
	virtual bool ReadLine(std::string &out) override {
		std::getline(std::cin, out);
		return (bool) std::cin;
	}
};

class FileInputStringStream : public TInputStringStream {
	std::ifstream in;

public:
	FileInputStringStream(std::string file) : in(file) {
	}

	virtual bool ReadLine(std::string &out) override {
		std::getline(in, out);
		return (bool) in;
	}
};

int main() {
	StdInInputStringStream i1;
	FileInputStringStream i2("s1");
	FileInputStringStream i3("s2");
	FileInputStringStream i4("s3");

	TInputStringStreamChain input;
	input
			.AddStream(i1)
			.AddStream(i2)
			.AddStream(i3)
			.AddStream(i4);

	std::string str;
	while (input.ReadLine(str)) {
		std::cout << str << "\n";
	}

	return 0;
}
