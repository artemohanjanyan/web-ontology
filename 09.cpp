#include <bits/stdc++.h>

// Задание 9
template <class TObject>
class TCopyContainer {
	// Довольно быстрая и легковесная структура данных, добавление за O(1).
	// При разрушении TCopyContainer разрушится forward_list,
	// который разрушит все хранимые unique_ptr, которые автоматически удалят хранимые объекты.
	std::forward_list<std::unique_ptr<TObject>> storage;

public:
	// Возращаемый указатель валиден до конца жизни объекта TCopyContainer.
	TObject *AddItem(const TObject &obj) {
		storage.push_front(std::make_unique<TObject>(obj));
		return storage.begin()->get();
	}
};


// ---------

int main()
{
	TCopyContainer<int> qq;
	auto q = qq.AddItem(123);

	std::cout << *q;

	return 0;
}
