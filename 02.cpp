#include <bits/stdc++.h>
using namespace std;

// Задание 2
// sizeof(a) вернёт размер указателя на char в байтах.
// Например, на моей машине (64-битная Ubuntu) это 8 байтов, на ideone.com это 4.
// sizeof для массива возвращает размер занимаемой им памяти в байтах.
// char всегда занимает 1 байт, пятибуквенный строковый литерал займёт 6 char-ов (5 символов + '\0').
// Следовательно, sizeof(b) вернёт 6.
// Пример вывода:
// 8 6


// ---------

int main()
{
	const char *a = "abcde";
	const char b[] = "abcde";
	cout << sizeof(a) << " " << sizeof(b) << endl;
	return 0;
}
